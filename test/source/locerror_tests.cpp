#define DOCTEST_CONFIG_NO_SHORT_MACRO_NAMES
#include <doctest/doctest.h>
#include <locerr/locerr.h>
#include <locerr/version.h>

#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "rclcpp/serialization.hpp"
#include "rclcpp/serialized_message.hpp"
#include "rcpputils/filesystem_helper.hpp"
#include "rosbag2_cpp/reader.hpp"
#include "rosbag2_cpp/readers/sequential_reader.hpp"
#include "rosbag2_cpp/writer.hpp"
#include "rosbag2_cpp/writers/sequential_writer.hpp"
#include "rosbag2_transport/storage_options.hpp"

using namespace locerr;

using LlasMsg = geometry_msgs::msg::Vector3Stamped;
auto SaveLlaToRosbag(const std::string& rosbag_dirpath, const std::vector<V3s>& llamsgs) {
  auto rosbag_directory = rcpputils::fs::path(rosbag_dirpath);
  rcpputils::fs::remove_all(rosbag_directory);
  rcpputils::fs::create_directories(rosbag_directory);

  rclcpp::Serialization<LlasMsg> serialization;
  rclcpp::SerializedMessage serialized_msg;

  rosbag2_cpp::Writer writer(std::make_unique<rosbag2_cpp::writers::SequentialWriter>());
  rosbag2_transport::StorageOptions storage_options;
  {
    const auto& uri = rosbag_directory.string();
    storage_options.uri = uri;
    storage_options.storage_id = "sqlite3";
  }
  rosbag2_cpp::ConverterOptions converter_options{};
  writer.open(storage_options, converter_options);  // close on scope exit
  rosbag2_storage::TopicMetadata tm;
  {
    tm.name = "/filter/positionlla";
    tm.type = "geometry_msgs/msg/Vector3Stamped";
    tm.serialization_format = "cdr";
  }
  writer.create_topic(tm);
  auto bag_message = std::make_shared<rosbag2_storage::SerializedBagMessage>();
  auto ret = rcutils_system_time_now(&bag_message->time_stamp);
  if (ret != RCL_RET_OK) {
    std::cout << "couldn't assign time rosbag message";
  }
  bag_message->topic_name = tm.name;
  for (auto&& llamsg : llamsgs) {
    serialization.serialize_message(&llamsg, &serialized_msg);
    bag_message->serialized_data = std::shared_ptr<rcutils_uint8_array_t>(
        &serialized_msg.get_rcl_serialized_message(), [](rcutils_uint8_array_t* /* data */) {});
    writer.write(bag_message);
  }
}

DOCTEST_TEST_CASE("Load lambert93 json file ground truth") {
  auto input{std::string("test/datatest/datatest0.json")};
  auto output{LoadLambert93<double>(input)};
  auto expected{Eigen::Matrix<double, 3, Eigen::Dynamic>(3, 3)};
  expected.col(0) << 10.10, 11.11, 12.12;
  expected.col(1) << 100.10, 110.11, 120.12;
  expected.col(2) << 1000.10, 1100.11, 1200.12;
  DOCTEST_CHECK_EQ(expected, output);
}
DOCTEST_TEST_CASE("Load Sensor data") {
  auto llamsgs = std::vector<V3s>(2);
  {
    llamsgs[0].vector.x = 1.1;
    llamsgs[0].vector.y = 2.2;
    llamsgs[0].vector.z = 3.3;
    llamsgs[1].vector.x = 11.1;
    llamsgs[1].vector.y = 12.2;
    llamsgs[1].vector.z = 13.3;
  }
  auto rosbagdir = std::string("test/datatest/rosbagdir");
  SaveLlaToRosbag(rosbagdir, llamsgs);
  auto rosbad_dir = "test/datatest/rosbagdir/rosbagdir_0.db3";
  auto output = FromBagToV3<double>(rosbad_dir, "/filter/positionlla");
  auto expected = Eigen::Matrix<double, 3, Eigen::Dynamic>(3, 2);
  // clang-format off
  expected<< 1.1, 11.1,
             2.2, 12.2,
             3.3, 13.3
             ;
  // clang-format on
  // auto expected = Wgs84s<double>{{1.1, 2.2, 3.3}, {11.1, 12.2, 13.3}};
  DOCTEST_CHECK_EQ(expected, output);
  rcpputils::fs::remove_all(rosbagdir);
}
DOCTEST_TEST_CASE("Distance") {
  auto input = Eigen::Matrix3Xd(3, 4);
  // clang-format off
  input <<  1,  4,  2,  21,
           -3,  2,  4, -12,
            3,  2,  1,  -1;
  // clang-format on
  auto output = Distance<double>(input);
  auto expected = Eigen::Matrix<double, 1, 4>();
  expected << 0, sqrt(9 + 25 + 1), sqrt(9 + 25 + 1) + sqrt(4 + 4 + 1),
      sqrt(9 + 25 + 1) + sqrt(4 + 4 + 1) + sqrt(19 * 19 + 16 * 16 + 4);
  DOCTEST_CHECK_EQ(output, expected);
  // clang-format on
}
DOCTEST_TEST_CASE("optim") {
  auto ecef = Eigen::Matrix3Xd(3, 5);
  // clang-format off
  ecef  << -1,  3,  7,  9, 10,
           -2,  0, -1, -3, -8,
           10, 12, 13, 15, 18;
  // clang-format on
  auto point = Eigen::Vector3d();
  point << 9, -3, 15;
  auto d = Distance<double>(ecef);
  auto problem = Problem<double>(ecef /* , d */);
  auto output = problem.Optim(point);
  auto expected = point;  // + distant
  DOCTEST_CHECK_LT((output.point - expected).norm(), 1e-6);
  DOCTEST_CHECK_LT((output.ecef_diff - Eigen::Vector3d::Zero()).norm(), 1e-6);
  // new point, reusing same problem var
  point << 3, 0, 12;
  output = problem.Optim(point);
  expected = point;
  DOCTEST_CHECK_LT((output.point - expected).norm(), 1e-6);
  DOCTEST_CHECK_LT((output.ecef_diff - Eigen::Vector3d::Zero()).norm(), 1e-6);
  // new mid point, checking for the direction this time
  point << 9.5, -5.5, 16.5;
  output = problem.Optim(point);
  expected = point;
  DOCTEST_CHECK_LT((output.point - expected).norm(), 1e-6);
  DOCTEST_CHECK_LT((output.ecef_diff - Eigen::Vector3d::Zero()).norm(), 1e-6);
  auto expected_dir = Eigen::Vector3d();
  expected_dir << 1, -5, 3;
  expected_dir.normalize();
  DOCTEST_CHECK_LT((output.rail_direction - expected_dir).norm(), 1e-6);
}
DOCTEST_TEST_CASE("Errors on path = 0") {
  auto path_l93 = std::string("test/datatest/datatest2.json");
  auto rosbagdir = std::string("test/datatest/rosbagdir");
  {
    auto l93s{LoadLambert93<double>(path_l93)};
    auto wgs84s{lamwgs::Lambert93ToWgs84(l93s)};  // todo: function
    auto r2d = 180. / M_PI;
    wgs84s.row(0) *= r2d;
    wgs84s.row(1) *= r2d;

    auto llamsgs = std::vector<V3s>(2);
    {
      llamsgs[0].vector.x = wgs84s(0, 3);
      llamsgs[0].vector.y = wgs84s(1, 3);
      llamsgs[0].vector.z = wgs84s(2, 3);
      llamsgs[1].vector.x = wgs84s(0, 0);
      llamsgs[1].vector.y = wgs84s(1, 0);
      llamsgs[1].vector.z = wgs84s(2, 0);
    }
    SaveLlaToRosbag(rosbagdir, llamsgs);
  }
  auto path_w84 = std::string("test/datatest/rosbagdir/rosbagdir_0.db3");
  auto expected = Eigen::Matrix3Xd::Zero(3, 2);
  // DOCTEST_SUBCASE("Ignoring alt") {  // todo: to ignore alt has no use case. to del
  //   auto [d1, d2, output, dy] = GtToSensor<double>(path_l93, path_w84 /* , false */);
  //   auto output_expected_diff = (output - expected).colwise().norm().maxCoeff();
  //   DOCTEST_CHECK_LT(output_expected_diff,
  //                    1e-2);  // 1cm precision..? worked better before rail ref switch?
  // }
  DOCTEST_SUBCASE("With alt") {
    auto [d1, d2, output, dy] = GtToSensor<double>(path_l93, path_w84 /* , true */);
    auto output_expected_diff = (output - expected).colwise().norm().maxCoeff();
    std::cout << output << std::endl;
    std::cout << "biggest error norm: " << output_expected_diff << std::endl;
    DOCTEST_CHECK_LT(output_expected_diff, 1e-6);
  }
  rcpputils::fs::remove_all(rosbagdir);
}
DOCTEST_TEST_CASE("Errors on altitude") {
  auto path_l93 = std::string("test/datatest/datatest3.json");
  auto rosbagdir = std::string("test/datatest/rosbagdir");
  {
    auto l93s = LoadLambert93<double>(path_l93);
    auto wgs84s = lamwgs::Lambert93ToWgs84(l93s);  // todo: function
    auto r2d = 180. / M_PI;
    wgs84s.row(0) *= r2d;
    wgs84s.row(1) *= r2d;
    auto llamsgs = std::vector<V3s>(2);
    {
      llamsgs[0].vector.x = wgs84s(0, 3);
      llamsgs[0].vector.y = wgs84s(1, 3);
      llamsgs[0].vector.z = wgs84s(2, 3);
      llamsgs[1].vector.x = wgs84s(0, 0);
      llamsgs[1].vector.y = wgs84s(1, 0);
      llamsgs[1].vector.z = wgs84s(2, 0) + 1.1;
    }
    SaveLlaToRosbag(rosbagdir, llamsgs);
  }
  auto path_w84 = std::string("test/datatest/rosbagdir/rosbagdir_0.db3");
  auto expected = Eigen::Matrix3Xd::Zero(3, 2).eval();
  // DOCTEST_SUBCASE("Ignoring alt") {
  //   auto [d1, d2, output, dy] = GtToSensor<double>(path_l93, path_w84 /* , false */);
  //   auto output_expected_diff = (output - expected).colwise().norm().maxCoeff();
  //   DOCTEST_CHECK_LT(output_expected_diff, 2e-3);  // before, 1e-6 ok
  // }
  DOCTEST_SUBCASE("With alt") {
    auto [d1, d2, output, dy] = GtToSensor<double>(path_l93, path_w84 /* , true */);
    expected.col(1) << 0, 0, 1.1;
    auto output_expected_diff = (output - expected).colwise().norm().maxCoeff();
    DOCTEST_CHECK_GT(output_expected_diff, 1e-6);
  }
  rcpputils::fs::remove_all(rosbagdir);
}

DOCTEST_TEST_CASE("Locerr version") {
  static_assert(std::string_view(LOCERR_VERSION) == std::string_view("1.0"));
  CHECK(std::string(LOCERR_VERSION) == std::string("1.0"));
}