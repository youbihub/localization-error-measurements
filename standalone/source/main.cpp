#include <locerr/locerr.h>
#include <locerr/version.h>
#include <matplot/matplot.h>

#include <Eigen/Core>
#include <cxxopts.hpp>
#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char** argv) {
  cxxopts::Options options(argv[0],
                           "A programm to compute errors between sensor localization (rosbag) and "
                           "some groundtruth path point list (json)");

  // clang-format off
  options.add_options()
    ("h,help", "Show help")
    ("v,version", "Print the current version number")
    ("r,rosbag", "Path to rosbag file",cxxopts::value<std::string>())
    ("j,json", "Path to json groundtruth file",cxxopts::value<std::string>())
    ("o,out", "output file",cxxopts::value<std::string>()->default_value("out.txt"))
    ("p,plot", "Plot some plots")
  ;
  // clang-format on

  auto result = options.parse(argc, argv);

  if (result["help"].as<bool>()) {
    std::cout << options.help() << std::endl;
    return 0;
  } else if (result["version"].as<bool>()) {
    std::cout << "Locerr, version " << LOCERR_VERSION << std::endl;
    return 0;
  }
  if (!result.count("rosbag") || !result.count("json")) {
    std::cout << "-h or --help to display function usage." << std::endl;
    return 0;
  }
  auto gt_path = result["json"].as<std::string>();
  auto rosbag_path = result["rosbag"].as<std::string>();
  auto [sensor, groundtruth, diff_xyz, diff_yaw]
      = locerr::GtToSensor<double>(gt_path, rosbag_path /* , true */);
  auto outpath = result["out"].as<std::string>();
  auto of = std::ofstream(outpath);
  auto interest_ouput{Eigen::MatrixX2d(diff_yaw.cols(), 2)};
  interest_ouput.col(0) = diff_xyz.row(1);
  interest_ouput.col(1) = diff_yaw * M_PI / 180;
  of << " normal err[m] angle err[deg]\n";
  of << interest_ouput.eval();

  if (result["plot"].as<bool>()) {
    matplot::figure();
    auto h = matplot::hist(diff_xyz.row(1).eval());
    matplot::title("Histogram of normal error (on ground plane) for MTi-G-710 automotive mode");
    matplot::xlabel("error distance [m]");
    matplot::ylabel("histogram bin count");

    matplot::figure();
    auto t = Eigen::VectorXd::LinSpaced(diff_xyz.cols(), 0, diff_xyz.cols() / 40);
    auto p1 = matplot::plot(t.eval(), diff_xyz.row(1).eval())->line_width(3);
    matplot::title("Normal error wrt. time for MTi-G-710 automotive mode");
    matplot::xlabel("time [s]");
    matplot::ylabel("normal ground error [m]");
    matplot::grid(matplot::on);

    matplot::figure();
    matplot::hold(matplot::on);
    auto& gt = groundtruth;
    // for (long int i = 0; i < groundtruth.cols(); i++) {
    //   gt.col(i) << groundtruth[i].lat, groundtruth[i].lon, 0;
    // }

    // to put in simplistic x-y for map representation
    auto latloc{gt(1, 0)};
    auto earth_radius{6371e3};
    sensor.row(0) *= earth_radius;
    gt.row(0) *= earth_radius;
    sensor.row(1) *= earth_radius * cos(latloc);
    gt.row(1) *= earth_radius * cos(latloc);
    // removing origin to have enough precision (probably float reduction in matplot++)
    auto ori = gt.col(0).eval();
    gt = gt.colwise() - ori;
    sensor = sensor.colwise() - ori;

    auto p2 = matplot::plot(gt.row(1).eval(), gt.row(0).eval());
    auto p3 = matplot::plot(sensor.row(1).eval(), sensor.row(0).eval());
    p2->line_width(3);
    p3->line_width(3);
    matplot::legend({"ground truth", "xsens MTi-G-710 automotive mode"});
    matplot::xlim({-2000, 20000});
    matplot::ylim({0, 8000});
    auto gca = matplot::gca();
    gca->axes_aspect_ratio(8. / 22);
    matplot::grid(matplot::on);
    matplot::xticks(matplot::iota(-2000, 100, 20000));
    matplot::yticks(matplot::iota(0, 100, 8000));
    matplot::title("Ground Truth and MTi-G-710 automotive mode on map");
    matplot::xlabel("west->east direction [m]");
    matplot::ylabel("south->north direction [m]");

    matplot::figure();
    auto h2 = matplot::hist((180. / M_PI * diff_yaw).eval());
    matplot::title("Histogram of heading (yaw) error for MTi-G-710 automotive mode");
    matplot::xlabel("error angle [deg]");
    matplot::ylabel("histogram bin count");

    matplot::figure();
    auto p4 = matplot::plot(t.eval(), (180 / M_PI * diff_yaw).eval())->line_width(3);
    matplot::title("Angle error wrt. time for MTi-G-710 automotive mode");
    matplot::xlabel("time [s]");
    matplot::ylabel("angle error [deg]");
    matplot::grid(matplot::on);
    matplot::show();
  }
  return 0;
}