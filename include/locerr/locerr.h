#ifndef LOCERR_LOCERR_H
#define LOCERR_LOCERR_H

#pragma once
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <vector>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ceres/ceres.h"
#pragma GCC diagnostic pop

#include <xsensdeviceapi.h>
#include <xstypes/xsthread.h>
// #include <xscontroller/xscontrol_def.h>
// #include <xscontroller/xsdevice_def.h>
// #include <xscontroller/xsscanner.h>
// #include <xstypes/xsdatapacket.h>
// #include <xstypes/xsoutputconfigurationarray.h>
// #include <xstypes/xsthread.h>
// #include <xstypes/xstime.h>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <atomic>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "ceres/rotation.h"
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "lambertgps/lambertgps.h"
#include "nlohmann/json.hpp"
#include "rclcpp/serialization.hpp"
#include "rclcpp/serialized_message.hpp"
#include "rosbag2_cpp/reader.hpp"
#include "rosbag2_cpp/readers/sequential_reader.hpp"
#include "rosbag2_transport/storage_options.hpp"

class CallbackHandler : public XsCallback {
public:
  CallbackHandler() : m_progress(0) {}

  virtual ~CallbackHandler() throw() {}

  int progress() const { return m_progress; }

protected:
  void onProgressUpdated(XsDevice* dev, int current, int total,
                         const XsString* identifier) override {
    (void)dev;
    (void)total;
    (void)identifier;
    m_progress = current;
  }

private:
  volatile std::atomic_int m_progress;
};

namespace locerr {

  // template <typename T> using Lambert93s = std::vector<lamwgs::Lambert93<T>>;

  // template <typename T> using Wgs84s = std::vector<lamwgs::Wgs84<T>>;

  template <typename T> auto LoadLambert93(const std::string& datapath) {
    if (!std::filesystem::exists(datapath)) {
      throw std::invalid_argument(datapath + " does not exist!");
    }
    nlohmann::json j;
    {
      std::ifstream i(datapath);
      i >> j;
    }
    auto lamb{Eigen::Matrix<T, 3, Eigen::Dynamic>(3, j.size())};
    int k = 0;
    for (size_t i = 0; i < j.size(); i++) {
      while (!j.contains(std::to_string(k))) {
        k++;
      }

      auto x{std::stod(j[std::to_string(k)]["coord_point"][0].get<std::string>())};
      auto y{std::stod(j[std::to_string(k)]["coord_point"][1].get<std::string>())};
      auto z{std::stod(j[std::to_string(k)]["coord_point"][2].get<std::string>())};
      lamb.col(i) << x, y, z;
      k++;
    }

    return lamb;
  }

  using V3s = geometry_msgs::msg::Vector3Stamped;
  using Q = geometry_msgs::msg::QuaternionStamped;
  template <typename U, typename T, int N> auto interp(const Eigen::Matrix<U, 1, Eigen::Dynamic>& X,
                                                       const Eigen::Matrix<U, N, Eigen::Dynamic>& Y,
                                                       const T& x) -> Eigen::Matrix<T, N, 1> {
    static long int idx = 0;
    auto dir{x > X(idx) ? 1 : -1};
    while (!(X(idx) <= x && x < X(idx + 1))) {
      if (idx + dir == X.cols() - 1 || idx + dir == -1) {
        break;
      }
      idx = idx + dir;
    }
    auto y{Y.col(idx) + (Y.col(idx + 1) - Y.col(idx)) * (x - X(idx)) / (X(idx + 1) - X(idx))};
    return y;
  }

  template <typename T>
  auto FromBagToV3(const std::string& rosbag_path, const std::string& topic_name) {
    rosbag2_cpp::Reader reader(std::make_unique<rosbag2_cpp::readers::SequentialReader>());
    rosbag2_transport::StorageOptions storage_options;
    const auto& uri{rosbag_path};
    storage_options.uri = uri;
    storage_options.storage_id = "sqlite3";
    rosbag2_cpp::ConverterOptions converter_options{"cdr", "cdr"};
    reader.open(storage_options, converter_options);
    reader.set_filter({{topic_name}});
    auto n40{1000000};  // 40 mins @400hz
    auto output{Eigen::Matrix3Xd(3, n40)};
    rclcpp::Serialization<V3s> serialization;
    uint k = 0;
    while (reader.has_next()) {
      auto bag_message{reader.read_next()};
      V3s v;
      rclcpp::SerializedMessage extracted_serialized_msg(*bag_message->serialized_data);
      serialization.deserialize_message(&extracted_serialized_msg, &v);
      output.col(k) << v.vector.x, v.vector.y, v.vector.z;
      k++;
    }
    output.conservativeResize(3, k);
    return output;
  }

  template <typename T>
  auto FromBagToQuat(const std::string& rosbag_path, const std::string& topic_name) {
    rosbag2_cpp::Reader reader(std::make_unique<rosbag2_cpp::readers::SequentialReader>());
    rosbag2_transport::StorageOptions storage_options;
    const auto& uri{rosbag_path};
    storage_options.uri = uri;
    storage_options.storage_id = "sqlite3";
    rosbag2_cpp::ConverterOptions converter_options{"cdr", "cdr"};
    reader.open(storage_options, converter_options);
    reader.set_filter({{topic_name}});
    auto n40{1000000};  // 40 mins @400hz
    auto output{Eigen::Matrix4Xd(4, n40)};
    rclcpp::Serialization<Q> serialization;
    uint k = 0;
    while (reader.has_next()) {
      auto bag_message{reader.read_next()};
      Q q;
      rclcpp::SerializedMessage extracted_serialized_msg(*bag_message->serialized_data);
      serialization.deserialize_message(&extracted_serialized_msg, &q);
      output.col(k) << q.quaternion.w, q.quaternion.x, q.quaternion.y, q.quaternion.z;
      k++;
    }
    output.conservativeResize(4, k);
    return output;
  }

  struct LlaQuat {
    Eigen::Matrix<double, 3, Eigen::Dynamic> lla;
    Eigen::Matrix<double, 4, Eigen::Dynamic> quat;
  };

  template <typename T> auto FromXsensToData(const std::string& logFileName) {
    XsControl* control = XsControl::construct();
    assert(control != 0);

    // Lambda function for error handling
    auto handleError = [=](std::string errorString) {
      control->destruct();
      std::cerr << errorString << std::endl;
      // return -1;
    };

    if (!control->openLogFile(logFileName))
      /* return  */ handleError("Failed to open log file. Aborting.");
    else
      std::cout << "Opened log file: " << logFileName.c_str() << std::endl;

    // Get number of devices in the file
    XsDeviceIdArray deviceIdArray = control->mainDeviceIds();
    XsDeviceId mtDevice;
    // Find an MTi device
    for (auto const& deviceId : deviceIdArray) {
      if (deviceId.isMti() || deviceId.isMtig()) {
        mtDevice = deviceId;
        break;
      }
    }

    if (!mtDevice.isValid()) /* return */
      handleError("No MTi device found. Aborting.");

    // Get the device object
    XsDevice* device = control->device(mtDevice);
    assert(device != nullptr);

    std::cout << "Device with ID: " << device->deviceId().toString() << " found in file."
              << std::endl;

    // By default XDA does not retain data for reading it back.
    // By enabling this option XDA keeps the buffered data in a cache so it can be accessed
    // through XsDevice::getDataPacketByIndex or XsDevice::takeFirstDataPacketInQueue
    device->setOptions(XSO_RetainBufferedData, XSO_None);

    // Load the log file and wait until it is loaded
    // Wait for logfile to be fully loaded, there are three ways to do this:
    // - callback: Demonstrated here, which has loading progress information
    // - waitForLoadLogFileDone: Blocking function, returning when file is loaded
    // - isLoadLogFileInProgress: Query function, used to query the device if the loading is done
    //
    // The callback option is used here.

    // Create and attach callback handler to device
    CallbackHandler callback;
    device->addCallbackHandler(&callback);

    std::cout << "Loading the file..." << std::endl;
    device->loadLogFile();
    while (callback.progress() != 100) xsYield();
    std::cout << "File is fully loaded" << std::endl;

    std::cout << "Exporting the data..." << std::endl;

    // Get total number of samples
    XsSize packetCount = device->getDataPacketCount();
    auto lla{Eigen::Matrix<double, 3, Eigen::Dynamic>{3, packetCount}};
    auto quat{Eigen::Matrix<double, 4, Eigen::Dynamic>{4, packetCount}};
    for (XsSize i = 0; i < packetCount; i++) {
      // Retrieve a packet
      XsDataPacket packet = device->getDataPacketByIndex(i);

      XsQuaternion quaternion = packet.orientationQuaternion();

      quat.col(i) << quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z();

      XsVector latLon = packet.latitudeLongitude();
      lla.col(i) << latLon[0], latLon[1], packet.altitude();
    }
    std::cout << "File is finished being parsed!" << std::endl;

    std::cout << "Freeing XsControl object..." << std::endl;
    control->destruct();

    std::cout << "Successful exit." << std::endl;
    return LlaQuat{lla, quat};
  }
  template <typename T> auto FromBagToData(const std::string& data_path) {
    return LlaQuat{FromBagToV3<T>(data_path, "/filter/positionlla"),
                   FromBagToQuat<T>(data_path, "/filter/quaternion")};
  }
  template <typename F> struct DistanceResidual {
    template <typename T> auto operator()(const T* const s, T* residual) const -> bool {
      auto r{Eigen::Map<Eigen::Matrix<T, 3, 1>>(residual)};
      auto p_i{interp<F, T, 3>(d, ecef, s[0])};
      r = target - p_i;
      return true;
    }
    const Eigen::Matrix<F, 3, Eigen::Dynamic>& ecef;
    const Eigen::Matrix<F, 1, Eigen::Dynamic>& d;
    Eigen::Matrix<F, 3, 1> target = Eigen::Matrix<F, 3, 1>();
  };
  template <typename T> auto Distance(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& m) {
    auto y{Eigen::Matrix<T, 1, Eigen::Dynamic>(1, m.cols())};
    auto d{m(Eigen::all, Eigen::seq(1, Eigen::last))
           - m(Eigen::all, Eigen::seq(0, Eigen::last - 1))};
    auto dist{d.array().square().colwise().sum().cwiseSqrt()};
    y(0) = 0;

    for (long int i = 0; i < y.cols() - 1; i++) {
      y(i + 1) = y(i) + dist(i);
    }
    return y;
  }
  template <typename T> struct Problem {
    Problem(const Eigen::Matrix<T, 3, Eigen::Dynamic>& e/* ,
            const Eigen::Matrix<T, 1, Eigen::Dynamic>& dist */)
        : ecef(e), d(Distance<double>(e)), costfunctor(new DistanceResidual<T>{ecef, d}) {
      options.linear_solver_type = ceres::DENSE_QR;
      // options.minimizer_progress_to_stdout = true;
      problem.AddResidualBlock(
          new ceres::AutoDiffCostFunction<DistanceResidual<T>, 3, 1>(costfunctor), NULL, &s);
    }

    ceres::Problem problem;
    ceres::Solver::Options options;
    ceres::Solver::Summary summary;

    const Eigen::Matrix<T, 3, Eigen::Dynamic>& ecef;  // xyz param
    const Eigen::Matrix<T, 1, Eigen::Dynamic> d;      // iterative distance of ecef points
    double s = 1;                                     // linear absissa
    DistanceResidual<T>* costfunctor;
    auto Optim(const Eigen::Matrix<T, 3, 1>& tgt) {
      costfunctor->target = tgt;
      ceres::Solve(options, &problem, &summary);

      auto p{interp<double, ceres::Jet<double, 1>, 3>(d, ecef, ceres::Jet<double, 1>(s, 0))};
      struct ResOptim {
        Eigen::Matrix<T, 3, 1> point;
        Eigen::Matrix<T, 3, 1> ecef_diff;
        Eigen::Matrix<T, 3, 1> rail_direction;
      };
      auto point{Eigen::Matrix<T, 3, 1>()};
      auto rail_direction{Eigen::Matrix<T, 3, 1>()};
      for (size_t i = 0; i < 3; i++) {
        point(i) = p[i].a;
        rail_direction(i) = p[i].v[0];
      }
      auto distance{costfunctor->target - point};
      return ResOptim{point, distance, rail_direction};
    }
  };

  template <typename T> auto REcefToEnu(const T& lat, const T& lon) {
    auto R{Eigen::Matrix<T, 3, 3>()};
    auto N{Eigen::Matrix<T, 3, 1>()};
    auto U{Eigen::Matrix<T, 3, 1>()};
    // clang-format off
    U << cos(lat) * cos(lon),
                    sin(lon),
         sin(lat);
    // clang-format on
    N << 0, 0, 1;
    N = N - N.dot(U) * U;
    N.normalize();
    auto E{N.cross(U)};
    R.col(0) = E;
    R.col(1) = N;
    R.col(2) = U;
    return R;
  }

  template <typename T>
  auto GtToSensor(const std::string& path_gt93, const std::string& data_path) {
    // load from files
    auto gt_lam93{LoadLambert93<T>(path_gt93)};

    auto lla_quat{LlaQuat{}};

    if (std::filesystem::path(data_path).extension() == ".db3") {
      // x_wgs84 = FromBagToV3<T>(data_path, "/filter/positionlla");
      // quat_enu = FromBagToQuat<T>(data_path, "/filter/quaternion");
      lla_quat = FromBagToData<T>(data_path);
    } else if (std::filesystem::path(data_path).extension() == ".mtb") {
      lla_quat = FromXsensToData<T>(data_path);
    } else
      std::cerr << "wrong extensions" << std::endl;

    auto& x_wgs84{lla_quat.lla};
    auto& quat_enu{lla_quat.quat};

    // topRows<2> function exists also in #include <Eigen/src/plugins/BlockMethods.h>
    x_wgs84(Eigen::seq(0, 1), Eigen::all) *= M_PI / 180.;  // lat/lon from deg to rad

    // convert to other relevant refs
    // auto gt_wgs84{lamwgs::Wgs84<T>(3, gt_lam93.cols())};
    // auto gt_ecef{lamwgs::Ecef<T>(3, gt_lam93.cols())};
    auto gt_wgs84{lamwgs::Lambert93ToWgs84(gt_lam93)};
    auto gt_ecef{lamwgs::Wgs84ToEcef(gt_wgs84)};
    // auto x_ecef{lamwgs::Ecef<T>(3, x_wgs84.cols())};
    auto x_ecef{lamwgs::Wgs84ToEcef(x_wgs84)};

    auto diff_xyz{Eigen::Matrix3Xd(3, x_wgs84.cols())};  // todo: enu also?
    auto diff_yaw{Eigen::Matrix<T, 1, Eigen::Dynamic>(1, x_wgs84.cols())};

    auto problem{Problem<T>(gt_ecef)};
    // loop for all data acquisition
    for (long int i = 0; i < x_wgs84.cols(); i++) {
      auto optimsol{problem.Optim(x_ecef.col(i))};

      // constructing Rotation from ecef to rail R0r to get diff
      auto R0r{Eigen::Matrix<T, 3, 3>()};
      {
        auto& x{optimsol.rail_direction};
        R0r.col(0) = x;
        auto z{x_ecef.col(i)};
        z -= z.dot(x) * x;
        z.normalize();
        R0r.col(2) = z;
        auto y{z.cross(x)};
        R0r.col(1) = y;
      }
      diff_xyz.col(i) = R0r.transpose() * optimsol.ecef_diff;

      //  compute diff yaw between sensor and rail
      // rail vect
      auto r_0_enu{REcefToEnu<T>(x_wgs84(0, i), x_wgs84(1, i))};
      auto rail_enu{(r_0_enu.transpose() * optimsol.rail_direction).eval()};
      rail_enu(2) = 0;  // projection on ground
      rail_enu.normalize();
      // sensor vect
      auto point{Eigen::Matrix<T, 3, 1>()};
      point << 1, 0, 0;
      auto sens_enu{Eigen::Matrix<T, 3, 1>()};
      if (quat_enu.size() > 0) {
        /* code */
        ceres::UnitQuaternionRotatePoint(quat_enu.col(i).data(), point.data(), sens_enu.data());
        sens_enu(2) = 0;
        sens_enu.normalize();
        // compute angle
        diff_yaw(i) = asin(rail_enu.cross(sens_enu)(2));
      }
    }

    struct data {
      Eigen::Matrix<T, 3, Eigen::Dynamic> sensor_lla;   // sensor lat lon alt
      Eigen::Matrix<T, 3, Eigen::Dynamic> groundtruth;  // ground truth lat lon alt
      Eigen::Matrix3Xd diff_xyz;                        // xyz sensor-rail in rail ref(rail left up)
      Eigen::Matrix<T, 1, Eigen::Dynamic> diff_yaw;  // angle between xsensor and rail in ENU ground
    };

    return data{x_wgs84, gt_wgs84, diff_xyz, diff_yaw};
  }

}  // namespace locerr

#endif